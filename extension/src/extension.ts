// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from "vscode";

//import DjangoTestAll from "./Commands/djangoTestAll";
//import DjangoTestClass from "./Commands/djangoTestClass";
import MDBTestFile from "./Commands/MDBTestFile";
//import DjangoTestFunction from "./Commands/djangoTestFunction";
import ICommand from "./Interfaces/ICommand";

const commands: ICommand[] = [
    new MDBTestFile(),
];

// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext) {

    // Register each command
    commands.forEach(command => {
        const disposable = vscode.commands.registerCommand(
            command.getCommandName(),
            command.execute,
            command,
        );
        context.subscriptions.push(disposable);
    });
}

// this method is called when your extension is deactivated
export function deactivate() {
    return;
}
