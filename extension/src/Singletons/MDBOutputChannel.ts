import * as vscode from "vscode";

/**
 * Singleton class to export a OutputChannel configured to use the polar channel
 * name.
 *
 * @export
 * @class MDBOutputChannel
 */
export default class MDBOutputChannel {

    public static getInstance(): vscode.OutputChannel {
        if (MDBOutputChannel.outputChannel) {
            return MDBOutputChannel.outputChannel;
        }

        MDBOutputChannel.outputChannel = vscode.window.createOutputChannel("MDB");
        return MDBOutputChannel.outputChannel;
    }

    public static destroyInstance(): void {
        if (MDBOutputChannel.outputChannel) {
            MDBOutputChannel.outputChannel.dispose();
            MDBOutputChannel.outputChannel = null;
        }
    }

    private static outputChannel: vscode.OutputChannel = null;

}
