import * as vscode from "vscode";

/**
 * A singleton class reference to an internal terminal reference
 *
 * @export
 * @class MDBTerminal
 */
export default class MDBTerminal {

    public static getInstance(): vscode.Terminal {
        if (MDBTerminal.instance) {
            return MDBTerminal.instance;
        }

        MDBTerminal.instance = vscode.window.createTerminal("MDB");
        MDBTerminal.instance.hide();
        return MDBTerminal.instance;
    }

    public static destroyInstance(): void {
        if (MDBTerminal.instance) {
            MDBTerminal.instance.dispose();
            MDBTerminal.instance = null;
        }
    }

    private static instance: vscode.Terminal = null;

}
