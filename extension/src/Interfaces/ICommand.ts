import * as vscode from "vscode";

interface ICommand {
    getCommandName(): string;
    execute(...args: any[]): Promise<void>;
}

export default ICommand;
