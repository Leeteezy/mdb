
interface ISpawnProcessCallbacks {
    stdout: (data: string | Buffer) => void;
    stderr: (data: string | Buffer) => void;
    exit: (code: string) => void;
}

export default ISpawnProcessCallbacks;
