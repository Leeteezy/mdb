import * as path from "path";
import * as vscode from "vscode";

import MDBTestHelper from "../Helpers/MDBTestHelper";
import DocumentHelper from "../Helpers/DocumentHelper";
import ICommand from "../Interfaces/ICommand";

/**
 * A test command for testing the current django test class.
 *
 * @export
 * @class DjangoTestClass
 * @extends {TestCommand}
 */
export default class MDBTestClass implements ICommand {

    public getCommandName(): string {
        return "mdb.test-class";
    }

    public async execute() {
        // Get the current editor and document
        const currentEditor = vscode.window.activeTextEditor;
        const currentDocument = currentEditor.document;

        if (!MDBTestHelper.isValidMDBTestDoucment(currentDocument)) {
            vscode.window.showErrorMessage(`is not a valid test file: [${currentDocument.fileName}]`);
            return;
        }

        // Get the python path to the current document.
        //const srcPath = MDBTestHelper.getDocumentPythonPath(currentDocument.fileName);

        const documentSymbols = await DocumentHelper.getDocumentSymbols(currentDocument);
        const targetSymbol = DocumentHelper.findSymbolForKindForCursorInEditor(
            documentSymbols,
            currentEditor,
            vscode.SymbolKind.Class,
        );

        if (!targetSymbol) {
            vscode.window.showWarningMessage("Cannot find valid test class symbol above the cursor");
            return;
        }

        const process = MDBTestHelper.runTests();
    }
}
