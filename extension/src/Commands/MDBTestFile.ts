import * as vscode from "vscode";

import MDBTestHelper from "../Helpers/MDBTestHelper";
import ICommand from "../Interfaces/ICommand";

/**
 * A test command for testing the current test file.
 *
 * @export
 * @class MDBTestFile
 * @extends {TestCommand}
 */
export default class MDBTestFile implements ICommand {
    public getCommandName(): string {
        return "mdb.test-file";
    }

    public async execute() {
        // Get the current editor and document
        const currentEditor = vscode.window.activeTextEditor;

        if (!currentEditor) {
            vscode.window.showErrorMessage("Please open a file to test");
            return;
        }

        const currentDocument = currentEditor.document;

        if (!currentDocument.fileName.includes(".asm") || !currentDocument.fileName.toLowerCase().includes(".s")) {
            vscode.window.showWarningMessage(`${currentDocument.fileName} is not a test file.`);
            return;
        }

        MDBTestHelper.runTests();
    }
}
