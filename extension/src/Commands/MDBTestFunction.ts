import * as path from "path";
import * as vscode from "vscode";

import MDBTestHelper from "../Helpers/MDBTestHelper";
import DocumentHelper from "../Helpers/DocumentHelper";
import ICommand from "../Interfaces/ICommand";
import OutputChannel from "../Singletons/MDBOutputChannel";

/**
 * A class for testing the current django unit test funciton above the cursor.
 *
 * @export
 * @class MDBTestFunction
 * @implements {ICommand}
 */
export default class MDBTestFunction implements ICommand {
    public getCommandName(): string {
        return "mdb.test-function";
    }

    public async execute() {
        // Get the current editor and document
        const currentEditor = vscode.window.activeTextEditor;
        const currentDocument = currentEditor.document;

        // Get the python path to the current document.
        //const srcPath = MDBTestHelper.getDocumentPythonPath(currentDocument.fileName);

        const documentSymbols = await DocumentHelper.getDocumentSymbols(currentDocument);
        const targetFunctionSymbol = DocumentHelper.findSymbolForKindForCursorInEditor(
            documentSymbols,
            currentEditor,
            vscode.SymbolKind.Function,
            (symbol) => symbol.name.toLowerCase().includes("test"),
        );

        const targetClassSymbol = DocumentHelper.findSymbolForKindForCursorInEditor(
            documentSymbols,
            currentEditor,
            vscode.SymbolKind.Class,
        );

        if (!targetClassSymbol) {
            vscode.window.showWarningMessage("Cannot find valid test class symbol above the cursor");
            return;
        }
        if (!targetFunctionSymbol) {
            vscode.window.showWarningMessage("Cannot find valid test function symbol above the cursor");
            return;
        }

        const outputChannel = OutputChannel.getInstance();

        const childProcess = MDBTestHelper.runTests();
    }
}
