import * as fs from "fs";
import * as path from "path";
import * as vscode from "vscode";

import MDBTestHelper from "../Helpers/MDBTestHelper";
import ICommand from "../Interfaces/ICommand";
import Output from "../Singletons/MDBOutputChannel";

/**
 * Runs all tests in a django project.
 *
 * @export
 * @class DjangoTestAll
 * @implements {ICommand}
 */
export default class MDBTestAll implements ICommand {
    public getCommandName() {
        return "mdb.test-all";
    }

    public async execute() {
        let packageName = "";
        // Try to get the module name from package.json
        try {
            const packageObject = await this.getPackageJsonObject();
            packageName = packageObject.name;
        } catch (err) {
            // If no package.json exists then try to use the rootPath's
            // basename as the module name.
            // i.e. ~/projects/polar.database.models/ will have a basename
            //      of polar.database.models
            packageName = path.basename(vscode.workspace.rootPath);
        }

        MDBTestHelper.runTests();
    }

    protected async getPackageJsonObject(): Promise<any> {
        const packagePath = path.join(vscode.workspace.rootPath, "package.json");
        return new Promise((resolve, reject) => {
            fs.readFile(packagePath, "utf-8", (err, data) => {
                let result = null;
                if (err) {
                    reject(err);
                    return;
                }
                try {
                    result = JSON.parse(data);
                } catch (exception) {
                    reject(exception);
                    return;
                }
                resolve(result);
            });
        });
    }
}
