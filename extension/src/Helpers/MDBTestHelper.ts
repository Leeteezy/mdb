import {ChildProcess, spawn} from "child_process";

import * as path from "path";
import * as vscode from "vscode";

import ISpawnProcessCallbacks from "../Interfaces/ISpawnProcessCallbacks";
import OutputChannel from "../Singletons/MDBOutputChannel";

export default class MDBTestHelper {

    /**
     * Runs the tests battery with the python scope path provided.
     *
     * @protected
     * @param {string} pythonPathScope A python path to the scope
     * @returns {Promise<CommandLineOutput>} A promise that resolves when the
     *                                       tests have finished running.
     *
     * @memberOf TestCommand
     */
    public static runTests(
        callbacks?: ISpawnProcessCallbacks): ChildProcess {

        const config = vscode.workspace.getConfiguration();

        const outputChannel = OutputChannel.getInstance();
        const mdb = MDBTestHelper.pathToMDB;
        const commandParts = [
            MDBTestHelper.mdbCommand,
            ...MDBTestHelper.mdbArgs,
        ];

        // Get the callback.
        const defaultCallbacks = MDBTestHelper.getDefaultTestProcessCallback();

        outputChannel.show();
        outputChannel.appendLine(`Spawning: ${mdb} ${commandParts.join(" ")}`);
        outputChannel.appendLine("Running Tests...");
        const env = process.env;
        const childProcess = spawn(mdb, commandParts, {
            cwd: `${vscode.workspace.rootPath}`,
            env,
        });

        callbacks = Object.assign({}, defaultCallbacks, callbacks);

        childProcess.stdout.on("data", callbacks.stdout);
        childProcess.stderr.on("data", callbacks.stderr);
        childProcess.on("exit", callbacks.exit);

        return childProcess;
    }

    /**
     * Takes a path that is inside the the workspace root and returns an array
     * of all of its components relative to the workspace root.
     *
     * The string is split on the `path.sep` character.
     *
     * @static
     * @param {string} documentPath The path to the document in the workspace.
     * @returns {string[]} An array of the parts of the path. If path not in
     *                     the workspace then returns null.
     *
     * @memberOf MDBTestHelper
     */
    public static getDocumentPathParts(documentPath: string): string[]|null {
        const relativePath = vscode.workspace.asRelativePath(documentPath);
        if (relativePath.startsWith("..")) {
            return null;
        }
        return relativePath.split(path.sep);
    }

    public static isValidMDBTestDoucment(document: vscode.TextDocument) {
        const pathParts = document.fileName.split(path.sep);
        const hasTestPart = pathParts.find((part) => part.toLowerCase().includes("test"));
        return hasTestPart;
    }

    private static pathToSrc = path.join(".");
    private static pathToMDB = path.join(".", "mdbdriver");
    private static mdbCommand = "test";
    private static mdbArgs = [
        `${MDBTestHelper.pathToSrc}`,
    ];

    private static failSeperatorRegex = /(^=+$)\n/gm;
    private static testFailRegex = /^[A-Z]+:\s(\w+)\s\(([\w|\.]+)\)/g;

    private static defaultStdoutCallback(data: string): void {
        const outputChannel = OutputChannel.getInstance();
        outputChannel.appendLine(`Info: ${data}`);
    }

    private static processOutputLine(line: string): void {
        const buffer: Buffer = Buffer.from(line);

        const outputChannel = OutputChannel.getInstance();
        if (line === ".") {
            outputChannel.append(line);
        } else if ( line === "S") {
            outputChannel.append(line);
        } else if (line === "E" || line === "F") {
            outputChannel.append(line);
        } else if (line.toLowerCase().startsWith("warning")) {
            outputChannel.appendLine(`WARN: ${line}`);
        } else {
            outputChannel.appendLine(line);
        }
    }

    private static getDefaultTestProcessCallback(): ISpawnProcessCallbacks {
        const errors: string[] = [];

        const defaultStderrCallback = (data: Buffer) => {
            const outputChannel = OutputChannel.getInstance();
            const dataString = data.toString("ascii");
            dataString
                .split("\n")
                .forEach(MDBTestHelper.processOutputLine);
            errors.push(dataString);
        };

        const defaultExitCallback = (code: Buffer | String) => {
            const outputChannel = OutputChannel.getInstance();
            outputChannel.appendLine(`Tests completed with exit code: ${code}`);
        };

        // if (err) {
        //     const errors = err.message.split(MDBTestHelper.failSeperatorRegex);
        //     const errorMatches = errors.map((error) => {
        //             return MDBTestHelper.testFailRegex.exec(error);
        //         })
        //         .filter((errorMatches) => {
        //             return !!errorMatches;
        //         })
        //         .forEach((errorMatches) => {
        //             outputChannel.appendLine(`Error On Test: ${errorMatches[2]}.${errorMatches[1]}`);
        //         });
        // }
        return {
            exit: defaultExitCallback,
            stderr: defaultStderrCallback,
            stdout: MDBTestHelper.defaultStdoutCallback,
        };
    }
}
