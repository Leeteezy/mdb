import * as vscode from "vscode";

export default class DocumentHelper {

    /**
     * Finds the symbol at the primary selection of the editor passed in.
     *
     * @protected
     * @param {vscode.SymbolInformation[]} symbols The symbol list to search in.
     * @param {vscode.TextEditor} editor The editor to pull the selection from.
     * @param {vscode.SymbolKind} symbolKind
     * @param {(symbol: vscode.SymbolInformation) => boolean} [additionalFilter]
     * @returns {vscode.SymbolInformation} The Symbol found at the cursor position or null if none exists.
     *
     * @memberOf TestCommands
     */
    public static findSymbolForKindForCursorInEditor(
        symbols: vscode.SymbolInformation[],
        editor: vscode.TextEditor,
        symbolKind: vscode.SymbolKind,
        additionalFilter?: (symbol: vscode.SymbolInformation) => boolean): vscode.SymbolInformation {
        const cursor = editor.selection.active;
        let symbolsOfKind = symbols
            .filter((symbol) => {
                return symbol.kind === symbolKind;
            });
        if (additionalFilter) {
            symbolsOfKind = symbolsOfKind
                .filter(additionalFilter);
        }
        const symbolRefsOfKindBeforeCursor = symbolsOfKind
            .map((symbol) => {
                return {
                    lineDifference: cursor.line - symbol.location.range.start.line,
                    symbol,
                };
            })
            .filter((symbolReference) => symbolReference.symbol.location.range.start.isBefore(cursor));

        if (symbolRefsOfKindBeforeCursor.length === 0) {
            return null;
        }

        const closestSymbol = symbolRefsOfKindBeforeCursor
            .reduce((carry, symbolReference) => {
                return carry.lineDifference < symbolReference.lineDifference ? carry : symbolReference;
            });

        return closestSymbol.symbol;
    }

    /**
     * An async function to get the symbols for a TextDocument
     *
     * @public
     * @param {vscode.TextDocument} document The document to get the symbols for.
     * @returns {Promise<vscode.SymbolInformation[]>} The list of symbols
     *
     * @memberOf ExecuteTestCommand
     */
    public static async getDocumentSymbols(document: vscode.TextDocument): Promise<vscode.SymbolInformation[]> {
        const symbols = await vscode.commands.executeCommand(
            "vscode.executeDocumentSymbolProvider",
            document.uri,
        );
        return <vscode.SymbolInformation[]> symbols;
    }
}
