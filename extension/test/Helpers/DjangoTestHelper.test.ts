import DjangoTestHelper from "../../src/Helpers/DjangoTestHelper";
import * as assert from "assert";

suite("Django Test Helper Tests", () => {
    test("Get Document Python Path", () => {
        const result = DjangoTestHelper.getDocumentPythonPath(
            "src/polar/ops/orderform/test/test_common.py",
        );
        assert.equal(result, "polar.ops.orderform.test.test_common");
    });

    test("Get Invalid Document Python Path", () => {
        const result = DjangoTestHelper.getDocumentPythonPath(
            "/var/log/system.log",
        );
        assert.equal(result, null);
    });
});
