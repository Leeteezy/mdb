# Polar: VSCode README

<!-- TOC depthFrom:2 -->

- [Features](#features)
    - [Polar: Run All Tests](#polar-run-all-tests)
    - [Polar: Run Test File](#polar-run-test-file)
    - [Polar: Run Test Class](#polar-run-test-class)
    - [Polar: Run Test Function](#polar-run-test-function)
- [Requirements](#requirements)

<!-- /TOC -->

The Polar VSCode Extension is an extension built for automating tasks and improving
workflow for users of VSCode.

## Features

Currently the main feature of *Polar: VSCode* is to auto mate test running via
4 main commands.

Each function expect `Polar: Run All Tests` will run a tests based on the
current open editor and the current active cursor position in the editor.

### Polar: Run All Tests

`Polar: Run All Tests` will run all tests for the current project by simply
running
```shell
$ ./bin/django test --keepdb --testrunner django.test.runner.DiscoverRunner --top-level-directory src/ {Project Name}
```
> The Package name is retrieve from the top level `package.json` files `.name` property.

### Polar: Run Test File

`Polar: Run Test File` will run the currently open python test file using the
following command.

```shell
$ ./bin/django test --keepdb --testrunner django.test.runner.DiscoverRunner --top-level-directory src/ {Python Path To File}
```

> You must have a test file as the active editor to run this command.

### Polar: Run Test Class

`Polar: Run Test Class` will run the currently open python test file scopeing
to the class the cursor is currently in and running the following command.

```shell
$ ./bin/django test --keepdb --testrunner django.test.runner.DiscoverRunner --top-level-directory src/ {Python Path To Class}
```

> You must have a test file currently open and the cursor must be inside a class
> inside that file.

### Polar: Run Test Function

`Polar: Run Test Function` will run the currently open python test file scopeing
to the function the cursor is currently inside of and running the following command

```shell
$ ./bin/django test --keepdb --testrunner django.test.runner.DiscoverRunner --top-level-directory src/ {Python Path To Function}
```

> You must have a test file currently open and active and the cursor must be
> inside a test function in that file.

## Requirements

This extension rely's on the [Python Extension] for vscode to pull the correct
document symbols out of the files for function and class lookups.

<!-- Links -->
[Python Extension]:https://marketplace.visualstudio.com/items?itemName=donjayamanne.python
<!-- /Links -->

