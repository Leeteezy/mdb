#include <iostream>
#include <sstream>
#include <memory>
#include <vector>
#include <algorithm>
#include <iomanip>
#include "bitreader.h"
#include "mdbcore.h"
using namespace std;
using uint=uint32_t;

template<typename T>
T get_input(string prompt = "Enter a value of", string error_prompt = "Incorrect input") {
    T val;
    cout << prompt << endl;
    while(!cin >> val) {
        cout << error_prompt << endl;
        cout << prompt << endl;
    }
    return val;
}

void breakpoint(Mips &mips, vector<uint>&, bool& break_next) {
    cout << "Breakpoint hit at " << mips.PC << endl;
    while(true){
        cout << "Input debugger command or type h for help" << endl;
        string command;
        cin >> command;
        if (command == "c" || command == "continue") {
            break_next=false;
            break;
        }
        else if (command == "n" || command == "next") {
            break_next = false;
            break;
        }
        else if (command == "p" || command == "print") {
            int index = 0;
            cout << "PC: " << mips.PC << endl;
            cout << "HI: " << mips.PC << endl;
            cout << "LO: " << mips.PC << endl;

            for (size_t i = 0; i < 32; ++i){
                cout << "$" << i << " = " << mips.registers[i] << endl;
                ++index;
            }
        }
        else if (command == "s" || command == "step") {
            break_next = true;
            break;
        }
        else if (command == "r" || command == "reverse") {
            auto stored = mips.past.back();
            mips.past.pop_back();
            mips.registers = stored.registers;
            mips.MEM = stored.MEM;
            mips.HI = stored.HI;
            mips.LO = stored.LO;
            mips.PC = stored.PC;
            mips.offset = stored.offset;
        }
        else if (command == "i" || command == "inspect") {
            while(true) {
                cout << "Memory inspection:" << endl
                     << "Enter 1 for an address or 2 for a register, else quit..." << endl;
                uint reg, i, val;
                int offset;
                cin >> i;
                if (i == 1) {
                    cout << "Enter value (in hex)" << endl;
                    cin >> hex >> val;
                    cout << mips.MEM[val] << endl;
                }
                else if (i == 2) {
                    cout << "Enter register (1-31)" << endl;
                    cin >> reg;
                    cout << "Enter offset (-2^31 to +2^31 - 1)" << endl;
                    cin >> offset;
                    cout << "REPLACE" << endl;
                    if (mips.MEM.find(mips.registers[reg] + offset) != mips.MEM.end())
                        cout << mips.MEM[mips.registers[reg] + offset] << endl;
                    else
                        cout << "Location " << mips.registers[reg] + offset << " is not initialized";
                }
                else break;
            }
        }
    }
}

void execute_program(Mips &mips){
    vector<uint> breakpoints;
    bool break_next = false;
    while(!mips.done){
        if (find(breakpoints.begin(), breakpoints.end(), mips.PC) != breakpoints.end()){
            breakpoint(mips, breakpoints, break_next);
        }
        else if(break_next){
            breakpoint(mips, breakpoints, break_next);
        }
        if(!mips.step()) break;
    }
}
void set_registers(Mips& mips){
    while(true){
        int reg;
        reg = get_input<int>("Enter register (-1 to quit): ");
        if(!cin >> reg)
        {
            cout << endl << "Invalid register" << endl;
        }
        if(reg == -1)
            break;
        cout << endl;
        if(reg > 31 || reg < 1)
        {
            cout << "Invalid register " << reg << endl;
            continue;
        }
        std::ostringstream prompt;
        prompt << "Enter initial value of register " << reg << ":";
        mips.registers[reg] = get_input<uint>(prompt.str(), "Invalid value...");
    }
}

uint hex_or_decimal(std::string input) {
    stringstream ss;
    uint val;
    ss << input;

    if(input.length() > 2 && input[0] == '0' && input[1] == 'x')
        ss >> hex >> val;
    else ss >> val;

    return val;
}

std::unordered_map<uint, uint> set_memory() {
    std::unordered_map<uint, uint> mem;
    std::string addr, val;
    uint addr_uint, val_uint;
    cout << "Enter address: ";
    cin >> addr;
    addr_uint = hex_or_decimal(addr);
    cout << "Enter value: " << endl;
    cin >> val;
    val_uint = hex_or_decimal(val);
    mem[addr_uint] = val_uint;
    return mem;
}


int main(int, char* argv[]) {
    Mips mips;
    BitStream b;
    cout << "Array(1) or two ints (2)" << endl;
    int i = -1;
    //while (!cin >> i || i != 0 || i != 1) {
    //  cout << "Enter something valid..." << endl;
    //}
    if (i == 0) {
        int length;
        cout << "Enter length of array" << endl;
        cin >> length;
        int *arr = new int[length];
        for (int k = 0; k < length; ++k) {
            int val;
            cin >> val;
            arr[k] = val;
        }
    }
    else {
        int a, b;
        cout << "Enter first int" << endl;
        cin >> a;
        cout << "Enter second int" << endl;
        cin >> b;
    }
    b.read_bits(argv[2]);
    mips.MEM = set_memory();
    set_registers(mips);
}


