#ifndef _MDBCORE_H
#define _MDBCORE_H

#include <array>
#include <unordered_map>
#include <stdint.h>
#include <vector>
#include "bitreader.h"

class Mips
{
  private:
    bool tracing = false;

    class StoredMemoryState {
      public:
        uint32_t PC, offset;
        int HI, LO;
        std::unordered_map<uint32_t, uint32_t> MEM;
        uint32_t registers[34];
        StoredMemoryState(Mips &m);
    };
  public:
    std::vector<Mips::StoredMemoryState> past;
    uint32_t registers[34];
    std::unordered_map<uint32_t, uint32_t> MEM;
    bool done = false;
    uint32_t PC = 0, offset = 0;
    uint32_t HI = 0, LO = 0;

    void trace(std::string instruction, std::string comment = "");

    bool step();

    void run(uint offset);

    void execute(uint32_t offset = 0);

    void store_state();

    void load_binary(BitStream stream, uint32_t offset);

    std::vector<uint32_t> code_to_words(BitStream code);

    void decode_execute(uint32_t instruction);
};

#endif
