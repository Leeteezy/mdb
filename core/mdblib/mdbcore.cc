#include <iostream>
#include <memory>
#include <vector>
#include <unordered_map>
#include <sstream>
#include "bitreader.h"
#include "mdbcore.h"
using namespace std;

template<typename T>
T normalize(T value)
{
  return value & 0xFFFFFFFF;
}

template<typename T>
T sign(T value) {
  return value & 0x80000000 ? value - 0x100000000 : value;
}

void Mips::run(uint32_t offset = 0) {
  this->PC = offset;
  while(this->step());
}

void Mips::execute(uint32_t offset) {
  PC = offset;
  while(this->step());
}

void Mips::store_state() {
  past.emplace_back(StoredMemoryState(*this));
}

void Mips::load_binary(BitStream binary, uint32_t offset) {
  if(offset % 4 != 0) throw runtime_error("Invalid offset, not aligned to word size 4");
  offset /= 4;
  cout << binary.read_index << endl;
}

vector<uint32_t> Mips::code_to_words(BitStream stream) {
    return stream.data;
}

void Mips::decode_execute(uint32_t instruction) {
  registers[0] = 0;
  uint32_t d = (instruction >> 11) & 0b11111;
  uint32_t s = (instruction >> 21) & 0b11111;
  uint32_t t = (instruction >> 16) & 0b11111;
  uint32_t i = instruction & 0b111111111111111111;
  if (i & 0x8000)
    i -= 0x10000;

  stringstream mips_instruction;
  stringstream comment;
  
  if ((instruction & 0b11111100000000000000011111111111) == 0b00000000000000000000000000100000) {
    registers[d] = normalize(registers[s] + registers[t]);
    mips_instruction << "add $" << d << ", $" << s << ", $" << t;
    comment << "$" << d << "=" << registers[d] << ", $" << s << "=" << registers[s] << ", $" << t << "=" << registers[t];
    this->trace(mips_instruction.str(), comment.str());
  }
  else if ((instruction & 0b11111100000000000000011111111111) == 0b00000000000000000000000000100010) {
    registers[d] = normalize(registers[s] - registers[t]);
    mips_instruction << "sub $" << d << ", $" << s << ", $" << t;
    comment << "$" << d << "=" << registers[d] << ", $" << s << "=" << registers[s] << ", $" << t << "=" << registers[t];
    this->trace(mips_instruction.str(), comment.str());
  }
  else if ((instruction & 0b11111100000000001111111111111111) == 0b00000000000000000000000000011000) {
    uint32_t result = sign(registers[s]) * sign(registers[t]);
    HI = normalize(result >> 32);
    LO = normalize(result);
    mips_instruction << "mult $" << s << ", $" << t;
    comment << "$" << s << "=" << registers[s] << ", $" << t << "=" << registers[t];
    this->trace(mips_instruction.str(), comment.str());
  }
  else if ((instruction & 0b11111100000000001111111111111111) == 0b00000000000000000000000000011001) {
    uint32_t result = registers[s] * registers[t];
    HI = normalize(result >> 32);
    LO = normalize(result);
    mips_instruction << "multu $" << s << ", $" << t;
    comment << "$" << s << "=" << registers[s] << ", $" << t << "=" << registers[t];
    this->trace(mips_instruction.str(), comment.str());
  }
  else if ((instruction & 0b11111100000000001111111111111111) == 0b00000000000000000000000000011010) {
    HI = normalize(sign(registers[s]) % sign(registers[t]));
    LO = normalize(sign(registers[s]) / sign(registers[t]));
    mips_instruction << "div $" << s << ", $" << t;
    comment << "$" << s << "=" << registers[s] << ", $" << t << "=" << registers[t];
    this->trace(mips_instruction.str(), comment.str());
  }
  else if ((instruction & 0b11111100000000001111111111111111) == 0b00000000000000000000000000011011) {
    HI = registers[s] % registers[t];
    LO = registers[s] / registers[t];
    mips_instruction << "divu $" << s << ", $" << t;
    comment << "$" << s << "=" << registers[s] << ", $" << t << "=" << registers[t];
    this->trace(mips_instruction.str(), comment.str());
  }
  else if ((instruction & 0b11111111111111110000011111111111) == 0b00000000000000000000000000010000) {
    registers[d] = HI;
    mips_instruction << "mfhi $" << d;
    comment << "$" << d << "=" << registers[d];
    this->trace(mips_instruction.str(), comment.str());
  }
  else if ((instruction & 0b11111111111111110000011111111111) == 0b00000000000000000000000000010010) {
    registers[d] = LO;
    mips_instruction << "mflo $" << d;
    comment << "$" << d << "=" << registers[d];
    this->trace(mips_instruction.str(), comment.str());
  }
  else if ((instruction & 0b11111111111111110000011111111111) == 0b00000000000000000000000000010100) {
    if (PC % 4 != 0) throw runtime_error("Address specified does not align with word size 4");
    registers[d] = MEM.find(PC / 4) != MEM.end() ? MEM[PC / 4] : 0;
    PC = normalize(PC + 4);
    mips_instruction << "list $" << d;
    comment << "$" << d << "=" << registers[d];
    this->trace(mips_instruction.str(), comment.str());
    mips_instruction << ".word" << registers[d];
    this->trace(mips_instruction.str());
  }
  else if ((instruction & 0b11111100000000000000000000000000) == 0b10001100000000000000000000000000) {
    uint32_t addr = normalize(registers[s] + i);
    if (addr % 4 != 0) throw runtime_error("Address specified does not align with word size 4");
    if (addr == (uint32_t)0xFFFF0004) {
      uint32_t val;
      std::cin >> val;
      registers[t] = val;
    }
    else {
      registers[t] = MEM.find(addr / 4) != MEM.end() ? MEM[addr / 4] : 0;
    }
    mips_instruction << "lw $" << t << ", " << i << "($" << s << ")";
    comment << "$" << t << "=" << registers[t] << ", $" << s << "=" << registers[s];
    this->trace(mips_instruction.str(), comment.str());
  }
  else if ((instruction & 0b11111100000000000000000000000000) == 0b10101100000000000000000000000000) {
    uint32_t addr = normalize(registers[s] + i);
    if (addr % 4 != 0) throw runtime_error("Address specified does not align with word size 4");
    if (addr == (uint32_t)0xFFFF000C) {
      //output to stdout
      cout << (registers[t] & 0xFF) << endl;
    }
    else {
      MEM[addr / 4] = registers[t];
    }
    mips_instruction << "sw $" << t << ", " << i << "($" << s << ")";
    comment << "$" << t << "=" << registers[t] << ", $" << s << "=" << registers[s];        
    this->trace(mips_instruction.str(), comment.str());
  }
  else if ((instruction & 0b11111100000000000000011111111111) == 0b00000000000000000000000000101010) {
    registers[d] = sign(registers[s]) < sign(registers[t]) ? 1 : 0;
    mips_instruction << "slt $" << d << ", " << s << ", " << t;
    comment << "$" << d << "=" << registers[d] << ", $" << s << "=" << registers[s] << ", $" << t << "=" << registers[t];
    this->trace(mips_instruction.str(), comment.str());
  }
  else if ((instruction & 0b11111100000000000000011111111111) == 0b00000000000000000000000000101011) {
    registers[d] = registers[s] < registers[t] ? 1 : 0;
    mips_instruction << "sltu $" << d << ", " << s << ", " << t;
    comment << "$" << d << "=" << registers[d] << ", $" << s << "=" << registers[s] << ", $" << t << "=" << registers[t];
    this->trace(mips_instruction.str(), comment.str());
  }
  else if ((instruction & 0b11111100000000000000000000000000) == 0b00010000000000000000000000000000) {
    if(registers[s] == registers[t])
      PC = normalize(PC = i * 4);
    mips_instruction << "beq $" << s << ", " << t << ", " << i;
    comment << "$" << d << "=" << registers[d] << ", $" << s << "=" << registers[s] << ", $" << t << "=" << registers[t];
    this->trace(mips_instruction.str(), comment.str());
  }
  else if ((instruction & 0b11111100000000000000000000000000) == 0b00010100000000000000000000000000) {
    if(registers[s] != registers[t])
      PC = normalize(PC = i * 4);
    mips_instruction << "bne $" << s << ", " << t << ", " << i;
    comment << "$" << d << "=" << registers[d] << ", $" << s << "=" << registers[s] << ", $" << t << "=" << registers[t];
    this->trace(mips_instruction.str(), comment.str());
  }
  else if ((instruction & 0b11111100000111111111111111111111) == 0b00000000000000000000000000001000) {
    PC = registers[s];
    mips_instruction << "jr $" << s;
    comment << "$" << s << "=" << registers[s];  
    this->trace(mips_instruction.str(), comment.str());
  }
  else if ((instruction & 0b11111100000111111111111111111111) == 0b00000000000000000000000000001001) {
    auto temp = registers[s];
    registers[31] = PC;
    PC = temp;
    mips_instruction << "jalr $" << s;
    comment << "$" << s << "=" << registers[s];  
    this->trace(mips_instruction.str(), comment.str());
  }
  else {
    stringstream errormsg;
    errormsg << "Unknown MIPS Instruction: " << instruction;
    throw runtime_error(errormsg.str());
  }
}
