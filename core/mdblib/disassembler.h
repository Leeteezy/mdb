#ifndef _DISASSEMBLE_H
#define _DISASSEMBLE_H

#include <iostream>
#include <fstream>

class MipsDisassembler {
    MipsDisassembler(std::ostream* o);
    std::ostream *out;
    std::istream *in;
    void use_file_output(std::string file_name);
    void use_file_input(std::string file_name);
     void use_standard_input();
    void use_standard_output();
    void help_text();
};

#endif