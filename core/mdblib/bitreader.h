#ifndef _BITREADER_H
#define _BITREADER_H

#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <stdint.h>

class BitStream {
  public:
    int64_t read_index = 0;
    std::vector<uint32_t> data;
    void read_bits(std::string file);
    uint64_t bit_count();
    bool read_bit();
};

#endif
