#include <iostream>
#include "disassembler.h"
#include "mdbcore.h"
using namespace std;

MipsDisassembler::MipsDisassembler(ostream *o = &cout) : out{o} {}

void MipsDisassembler::help_text() {
  *out << "Usage: [FILE] < [ASSEMBLED FILE] > [DISASSEMBLED FILE]" << endl;
}

void MipsDisassembler::use_file_output(string file_name)
{
  ofstream output(file_name);
  out = &output;
}

void MipsDisassembler::use_standard_output()
{
  out = &cout;
}

void MipsDisassembler::use_file_input(string file_name)
{
  ifstream input(file_name);
  in = &input;
}

void MipsDisassembler::use_standard_input()
{
  in = &cin;
}

void disassemble() {
  
}
