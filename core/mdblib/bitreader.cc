#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>
#include "bitreader.h"
using namespace std;
using uint = uint32_t;

void BitStream::read_bits(string file) {
  ifstream input(file, std::ifstream::binary);
  if (!input) {
    stringstream msg;
    msg << "Specified file \"" << file << "\" failed to open";
    throw new runtime_error(msg.str());
  }
  uint32_t j [1];
  do {
    j[0] = 0;
    input.read((char*)j, sizeof(j));
    if (input) {
      data.push_back(j[0]);
    }
    else {
      if(input.eof())
        break;
      else {
        stringstream msg;
        msg << "Specified file \"" << file << "\" could not be parsed as a collection of 32 bit words";
        throw new runtime_error(msg.str());
      }
    }
  } while(true);
}

uint64_t BitStream::bit_count() {
  return data.size() * sizeof(uint);
}

bool BitStream::read_bit() {
  bool bit = (data[read_index / sizeof(uint)]) >> (read_index % sizeof(uint)) & 1;
  ++read_index;
  return bit;
}
