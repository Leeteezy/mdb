#include "bitreader.h"
#include <iostream>
using namespace std;

int main() {
  BitStream stream;
  stream.read_bits("test.txt");
  for(auto i : stream.data) {
      cout << i << endl;
  }
}
