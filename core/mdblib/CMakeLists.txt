cmake_minimum_required(VERSION 3.7)
project(mdb)

set(CMAKE_CXX_STANDARD 14)

set(SOURCE_FILES mdbcore.cc bitreader.cc disassembler.cc mdb_driver.cc)
add_executable(mdb ${SOURCE_FILES})
