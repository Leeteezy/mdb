extern crate gcc;

fn main() {
    gcc::compile_library("libmdbcore.a", &["mdblib/mdbcore.cc"]);
    gcc::compile_library("libbitreader.a", &["mdblib/bitreader.cc"]);
    gcc::compile_library("libdisassembler.a", &["mdblib/disassembler.cc"]);
    println!("cargo:rustc-flags=-l dylib=stdc++");
}
